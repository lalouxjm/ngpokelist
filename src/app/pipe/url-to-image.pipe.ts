import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'urlToImage'
})
export class UrlToImagePipe implements PipeTransform {

  transform(url: string): string {
    const parts = url.split('/');
    const id = parts[parts.length -2];
    return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+ id + '.png';
  }

}
