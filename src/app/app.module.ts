import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbInputModule, NbCardModule, NbMenuModule, NbSidebarModule, NbButtonModule, NbIconModule, NbListModule, NbCheckboxModule, NbDialogModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { HomeComponent } from './pages/home/home.component';
import { PokelistComponent } from './pages/pokelist/pokelist.component';
import { ListComponent } from './pages/pokelist/pages/list/list.component';
import { DetailsComponent } from './pages/pokelist/pages/details/details.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { UrlToImagePipe } from './pipe/url-to-image.pipe';
import { LoaderRadarComponent } from './components/loader-radar/loader-radar.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PokelistComponent,
    ListComponent,
    LoaderRadarComponent,
    DetailsComponent,
    UrlToImagePipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    NbThemeModule.forRoot({ name: 'cosmic' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbCardModule,
    NbInputModule,
    NbIconModule,
    NbListModule,
    NbCheckboxModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
