import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pokeNg';

  links: NbMenuItem[] = [
    { link: '/home', title: 'Home Page', icon: 'home' },
    { link: '/pokelist', title: 'List of Pokemons', icon: 'globe' },
  ]
}
