import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { ListPokemon } from 'src/app/models/list-pokemon.model';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  pokeList: ListPokemon|null = null;
  pokemonUrl!:string;

  destroyed: Subject<boolean> = new Subject<boolean>();

  constructor(
    private readonly _listService: ListService
  ){}

  ngOnInit(): void {
    this._listService.pokeList$
      .pipe(takeUntil(this.destroyed))
      .subscribe(list => {
        this.pokeList = list      
    });
  }

  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }

  prev(){
    if(!this.pokeList?.previous){
      return;
    }
    this._listService.load(this.pokeList?.previous)
  }

  next(){
    if(!this.pokeList?.next){
      return;
    }
    this._listService.load(this.pokeList?.next)
  }

  onClick(url: string){
    this._listService.loadDetails(url);

  }

  


}
