import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs';
import { DestroyedComponent } from 'src/app/core/destroyed.component';
import { PokemonDetail } from 'src/app/models/pokemon.model';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent extends DestroyedComponent implements OnInit {

  pokemon: PokemonDetail|null = null;

constructor(
  private readonly _listService: ListService
){
  super()
}

ngOnInit(): void{
  this._listService.pokeDetail$
    .pipe(takeUntil(this.destroyed$))
    .subscribe(s => this.pokemon = s)
}

}
