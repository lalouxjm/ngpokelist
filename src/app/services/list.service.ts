import { Injectable } from '@angular/core';
import { PokemonDetail } from '../models/pokemon.model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ListPokemon } from '../models/list-pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private _pokeList$: BehaviorSubject<ListPokemon|null> = new BehaviorSubject<ListPokemon|null>(null);

  get pokeList$(){
    return this._pokeList$.asObservable();
  }

  private _pokeDetail$: BehaviorSubject<PokemonDetail|null> = new BehaviorSubject<PokemonDetail|null>(null);

  get pokeDetail$(){
    return this._pokeDetail$.asObservable();
  }

  constructor(
    private readonly _httpClient: HttpClient
  ){
    this.load();
  }

  load(url: string = 'https://pokeapi.co/api/v2/pokemon'){
    console.log(url);
    this._httpClient.get<ListPokemon>(url, { reportProgress: true }).subscribe(list => {
      this._pokeList$.next(list);
    })
  }

  loadDetails(url: string){
    this._httpClient.get<PokemonDetail>(url, { reportProgress: true }).subscribe(list => {
      this._pokeDetail$.next(list);
    })
  }
}
